import PySimpleGUI as sg

class interfaceClass:
    # constructor declare elements
    def __init__ (self):
        self.source_input = sg.InputText('', size=(70,1), readonly= True, enable_events=True, key='source_file')
        self.source_button = sg.FileBrowse('Select Source', size=(20,1), target='source_file', file_types=(("Python", "*.py"),), tooltip='File for conversion from py to exe')
        self.env_input = sg.InputText('', size=(70,1), readonly= True, enable_events=True, key='env_folder')
        self.env_button = sg.FolderBrowse('Select Environment', size=(20,1), target='env_folder',tooltip='[Optional] Lookup modules, default system path')
        self.process_button = sg.Button('Process', size=(20,1), key='process', disabled=True)
        self.sf = sg.Checkbox('single file', key='option_sf',  enable_events=True, tooltip='[Optional] Compiled into single exe. However requires time to unpack backend')
        self.nc = sg.Checkbox('No console', key='option_nc',  enable_events=True, tooltip='[Optional] Compiled without console')
        self.text_area = sg.Multiline(size=(95,5), key='-ML-', autoscroll=True, disabled= True)
        self.reset = sg.Button('Reset', size=(20,1), key='reset')

        
    # layout declaration
    def layout(self):
        return [
                [sg.Frame('Souce Target', 
                    [
                        [self.source_input, self.source_button]
                    ])],
                [sg.Frame('Environment Target', 
                    [
                        [self.env_input, self.env_button],
                    ])],
                
                [self.sf, self.nc],
                [self.text_area],
                [self.process_button, self.reset]
            ]
    
    # load element on window
    def load(self):
        return sg.Window('PySimpleInstaller 0.0.0').Layout(self.layout())
    
    # popup message
    def popup(self, message):
        sg.popup_ok(message)
    
    def popup_yesno(self, message):
        return sg.popup_yes_no(message)  # Shows Yes and No buttons

    # tray message
    def tray(self, message):
        sg.SystemTray.notify('Notification', message )
        
    def clear(self):
        self.source_input.Update('')
        self.env_input.Update('')
        self.text_area.Update('')
        self.sf = False
        self.nc = False
        self.process_button.Update(disabled=True)
        self.process_button.Update('Process')
        