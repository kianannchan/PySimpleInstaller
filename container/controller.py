import subprocess
import os, time
from pathlib import Path

class Model:
    def __init__(self):

        if os.name  == 'nt':
            self.EXT = 'bat'
            self.REMDIR = 'rmdir /Q /S'
            self.DELFILE = 'del'
        else:
            #TODO validiate linux environment
            self.EXT = 'sh'
            self.REMDIR = 'rm -rf'
            self.DELFILE = 'rm -f'
            pass
        self.genFile = 'gen.%s' % (self.EXT) 
        self.filePath = ''
        self.filename = ''
        self.para = [False, False]
        self.env = ''
        self.timestamp = int(time.time())
        

class controller(Model):

    def setFilepath(self,file):
        self.filePath = file
        self.filename = Path(self.filePath).stem
    def getFilepath(self):
        return self.filePath
    def setParameter(self,para):
        self.para = para
    def getParameter(self):
        return self.para
    def setEnvironment(self,env):
        self.env =env
    def getEnvironment(self):
        return self.env
    
    def preparePyinstallerStatement(self):
        """
        Info: Prepare pyinstaller statement\\
        Para: None\\
        Class para: filePath ,filename, timestamp, env, para\\
        Output: Return concat statement in string format
        """
        statement = 'pyinstaller "%s"' % (self.filePath)
        statement = statement + ' --distpath output\\%s_%s' % (self.filename, self.timestamp)
        statement = statement + ' --noconfirm'
        if len(self.env) > 0:
            statement = statement + ' --paths %s' % (self.env)
        if self.para[0] == True:
            statement = statement + ' --onefile'
        if self.para[1] == True:
            statement = statement +' --noconsole'
        return statement

    def prepareBATstatment(self):
        """
        Info: Prepare Bat statement\\
        Para: None\\
        Class para: env\\
        Output: Return concat statement in string format
        """
        setEnv = ''
        if len(self.env) > 0:
            setEnv = 'cd %s\n' % self.env
            setEnv = setEnv + 'cd ..\\..\n'
            setEnv = setEnv + 'Scripts\\activate\n'        
        coreFile = self.preparePyinstallerStatement()
        removeBuildFile = '%s "build"' % (self.REMDIR)
        remove__pycache__File = '%s "__pycache__"' % (self.REMDIR)
        removeSpec = '%s "%s.spec"' % (self.DELFILE, self.filename)
        finalStatement = '%s\n%s\n%s\n%s\n%s' % (setEnv, coreFile, removeBuildFile, remove__pycache__File, removeSpec)
        return finalStatement
    
    def writeBat(self):
        """
        Info: Write Bat File\\
        Para: None\\
        Class para: genFile\\
        Output: Bat File with prepared statement
        """
        try:
            f= open(self.genFile,"w+")
            f.write(self.prepareBATstatment())
            f.close()
        except Exception as ex:
            print(ex)

    def execute(self):
        """
        Info: Execute Bat File in sequenial\\
        Para: None\\
        Class para: genFile\\
        Output: Execution of Bat File
        """
        try:
            subprocess.call(self.genFile)
            os.remove(self.genFile)
                    
        except Exception as ex:
            print(ex)
    
    def openOutput(self):
        """
        Info: Open processed output folder\\
        Para: None\\
        Class para: None\\
        Output: Open folder
        """
        try:
            if self.para[0] == True:
                output_path = 'output\\%s_%s' % (self.filename, self.timestamp)
            else:
                output_path = 'output\\%s_%s\\%s' % (self.filename, self.timestamp, self.filename)
            subprocess.check_call(['explorer', output_path])
        except Exception as ex:
            print(ex)
            
    def pyinstaller(self, ops):
        """
        Info: Determine or Install pyinstaller according to ops\\
        Para: ops\\
        Class para: None\\
        Output: Install or Check pyinstaller
        """
        try:
            exist = subprocess.check_call('pip %s pyinstaller' % ops)
            return exist
        except Exception as ex:
            print(ex)
            

if __name__ == "__main__":
    obj = controller()
