import interface
import controller
import sys
interfaceObj = interface.interfaceClass()
controlObj = controller.controller()

# check for pyinstaller and install if necessary
def pyinstallercheck():
    exist = controlObj.pyinstaller('show')
    if exist == None:
        response = interfaceObj.popup_yesno('pyinstaller not installed on this system, process to install?')
        if response == 'Yes':
            controlObj.pyinstaller('install')
        else:
            interfaceObj.popup('Cannot proceed w/o pyinstaller. Program will now close!')
            sys.exit()


# load Interface obj
window = interfaceObj.load()

# check for pyinstaller existence
pyinstallercheck()

while True:
    # listen for event emitter and its value        
    event, values = window.Read()
    command = interfaceObj.text_area

    # determine pyinstaller options parameter selection
    if event == 'option_nc' or event == 'option_sf':
        option_nc = (values['option_nc'])
        option_sf = values['option_sf']
        controlObj.setParameter([option_sf,option_nc])
        command.Update(controlObj.preparePyinstallerStatement())
    
    # determine source file selection    
    elif event == 'source_file':
        source_file = values['source_file']
        process_button = interfaceObj.process_button
        if len(values['source_file']) > 0:
            if controlObj.getFilepath() != source_file:
                controlObj.setFilepath(source_file)
                process_button.Update(disabled=False)
                command.Update(controlObj.preparePyinstallerStatement())
                process_button.Update('Process')

    # determine env folder selection
    elif event == 'env_folder':
        if len(values['env_folder']) > 0:
            env_folder = values['env_folder']
            if '/Lib/site-packages' in (env_folder):
                controlObj.setEnvironment(env_folder)
                command.Update(controlObj.preparePyinstallerStatement())
            else:
                interfaceObj.popup('Invalid Environment, Path nesting \Lib\site-packages')
                interfaceObj.env_input.Update('')
    
    # clear all fields and obj to default state        
    elif event == 'reset':
        controlObj = controller.controller()
        interfaceObj.clear()

    # execute the pyinstaller ops
    elif event == 'process':
        process_button = interfaceObj.process_button
        if process_button.GetText() == 'Process':
            controlObj.writeBat()
            controlObj.execute()
            interfaceObj.popup('Done executing!')
            process_button.Update('Open')
        # open output folder
        else:
            controlObj.openOutput()
    # detect close program
    if event in (None, 'Exit'):
        break

window.close()