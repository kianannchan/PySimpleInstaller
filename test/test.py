import unittest
import sys
sys.path.append('../container')
from controller import controller  
from pathlib import Path
import os, shutil



class SimpleTest(unittest.TestCase): 
    
    def setUp(self):
        self.obj = controller()
        self.obj.setFilepath('../container/view.py')

    # Test for default
    def testCore_default(self):
        pyinstaller = (self.obj.prepareBATstatment().splitlines())[1]
        if '--onefile' not in pyinstaller and '--noconsole' not in pyinstaller:
            status = True
        else:
            status = False
        self.assertEqual(status, True)
        
    # Test for single file
    def testCore_sf(self):
        self.obj.setParameter([True, False])
        pyinstaller = (self.obj.prepareBATstatment().splitlines())[1]
        if '--onefile' in pyinstaller and '--noconsole' not in pyinstaller:
            status = True
        else:
            status = False
        self.assertEqual(status, True)
        
    # Test for no console
    def testCore_nc(self):
        self.obj.setParameter([False, True])
        pyinstaller = (self.obj.prepareBATstatment().splitlines())[1]
        if '--onefile' not in pyinstaller and '--noconsole' in pyinstaller:
            status = True
        else:
            status = False
        self.assertEqual(status, True)
    
    # Test output exist when single file is True  
    def testIntegration_sf(self):
        self.obj.setParameter([True, False])
        self.obj.writeBat()
        self.obj.execute()
        if self.obj.para[0] == True:
                    output_path = 'output\\%s_%s' % (self.obj.filename, self.obj.timestamp)
        else:
                output_path = 'output\\%s_%s\\%s' % (self.obj.filename, self.obj.timestamp, self.obj.filename)
        fileList = os.listdir('%s' % (output_path))
        self.assertTrue(len(fileList) == 1)
    
    # Test output exist when single file is False
    def testIntegration_notsf(self):
        self.obj.setParameter([False, False])
        self.obj.writeBat()
        self.obj.execute()
        if self.obj.para[0] == True:
            output_path = 'output\\%s_%s' % (self.obj.filename, self.obj.timestamp)
        else:
            output_path = 'output\\%s_%s\\%s' % (self.obj.filename, self.obj.timestamp, self.obj.filename)
        fileList = os.listdir('%s' % (output_path))
        self.assertTrue(len(fileList) > 1)
    
    # Remove Test output    
    def tearDown(self):
        if (os.path.isdir('output')):
            shutil.rmtree('output')


if __name__ == '__main__': 
    unittest.main() 

    

            
